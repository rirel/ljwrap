
# LuaJIT paths
INC= $(HOME)/local/include/luajit-2.0
LIB= $(HOME)/local/lib

# compiler/linker settings
CC= clang
CFLAGS= -g
LDFLAGS= -macosx_version_min 10.10 -pagezero_size 10000 \
         -image_base 100000000 -lluajit-5.1 -lc -arch x86_64 \
         -L$(LIB)

# build parameters
OBJ= main.c.o lib.l.o
TARGET= main

# build rules
.SILENT:
all: init.bc build
	echo $(POK)
build: $(OBJ)
	echo $(PLD) $(OBJ)
	ld $(LDFLAGS) $(OBJ) -o $(TARGET)
%.c.o: %.c
	echo $(PCC) $<
	$(CC) -c $< -I$(INC) $(CFLAGS) -o $@
%.bc: %.lua
	echo $(PLL) $<
	luajit -b -t h $< $@
%.l.o: %.lua
	echo $(PLL) $<
	luajit -b -t o $< $@

.PHONY: clean
clean:
	echo $(POK)
	rm *.o *.bc $(TARGET) 

PCC= "\033[33mCC\033[0m"
PLL= "\033[33mLL\033[0m"
PLD= "\033[33mLD\033[0m"
POK= "\033[32mOK\033[0m"
