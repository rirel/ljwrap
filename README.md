# LuaJIT Wrapper #
This is a general-purpose template application which can be forked to create
a clean, hybrid Lua/C CLI tool (or even GUI app). It contains an example
(useless) library stored in `lib.lua`. You'll have to edit the `Makefile` if
you want to build without this or add additional compile-time libraries.

## Anatomy of ljwrap ##
The core app logic should be in `init.lua`, the contents of which are executed
when the wrapper starts. Additional functionality should be bundled into
LuaJIT native object libraries and linked with main into the final binary so
they can be `require`'d from within `init.lua` (or even other libraries).

C functions, if needed, can be added in `main.c`. Functions in question must
be of the `lua_CFunction` type, i.e. take a single `lua_State *` and return
an `int` representing the number of stack elements that should be handed to
the Lua engine. They can be made accessible to Lua code in a few different
ways, the easiest of which (usually) is to use `luaL_register`.
