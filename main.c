#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include "init.bc"

int main(int argc, char **argv) {
	lua_State *L = luaL_newstate();
	luaL_openlibs(L);
	luaL_loadbuffer(L, luaJIT_BC_init, luaJIT_BC_init_SIZE, "init");
	lua_pcall(L, 0, LUA_MULTRET, 0);
	lua_close(L);
	return 0;
}
